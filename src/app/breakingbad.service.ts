import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Character } from './character';

@Injectable({
  providedIn: 'root'
})
export class BreakingBadService {

  constructor(private http: HttpClient) { }

  getCharacter(id: number|string): Observable<Character> {
    return this.http.get('https://www.breakingbadapi.com/api/characters/'+id)
      .pipe( map(resp => resp as Character) );
  }
}
