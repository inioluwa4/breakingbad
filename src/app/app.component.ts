import { Component } from '@angular/core';
import {BreakingBadService } from './breakingbad.service';
import { Character } from './character';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'our-app';
  charId: number;
  character: Character;


  constructor(private char: BreakingBadService) {}

  submit(): void {
    this.char.getCharacter(this.charId).subscribe(
      (data) => {
        this.character = data[0];
        console.log(this.character);
      }
    );
  }
}

