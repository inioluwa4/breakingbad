import { Component, OnInit, Input } from '@angular/core';
import { BreakingBadService } from '../breakingbad.service';
import { Character } from '../character';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.css']
})
export class CharactersComponent implements OnInit {
  @Input() character: Character ;

  constructor(private charService: BreakingBadService) { }

  ngOnInit(): void {
    this.charService.getCharacter(1).subscribe(
      (char) => {
        this.character = char;
      });
  }
  getNext(): void {
    this.charService.getCharacter(this.character.char_id+1).subscribe(
      (char) => {
        this.character = char;
      });
  }

}
